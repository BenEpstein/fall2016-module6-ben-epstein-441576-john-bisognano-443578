// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	url = require('url'),
	path = require('path'),
	mime = require('mime'),
	path = require('path'),
	fs = require("fs");

// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	var filename = path.join(__dirname, "files", url.parse(req.url).pathname);
	(fs.exists || path.exists)(filename, function(exists){
		if (exists) {
			fs.readFile(filename, function(err, data){
				if (err) {
					// File exists but is not readable (permissions issue?)
					resp.writeHead(500, {
						"Content-Type": "text/plain"
					});
					resp.write("Internal server error: could not read file");
					resp.end();
					return;
				}

				// File exists and is readable
				var mimetype = mime.lookup(filename);
				resp.writeHead(200, {
					"Content-Type": mimetype
				});
				resp.write(data);
				resp.end();
				return;
			});
		}else{
			// File does not exist
			resp.writeHead(404, {
				"Content-Type": "text/plain"
			});
			resp.write("Requested file not found: "+filename);
			resp.end();
			return;
		}
	});
});
app.listen(3456);

// Do the Socket.IO magic:
var io = socketio.listen(app);
var users = {}; //to store all of the users in the chatroom
var rooms = {};
// var roomsWithPass = {};
var userRoom = {};
var Files = {};
rooms["waiting"] = "";

io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.

	//adding users


	socket.on('addUser', function(user){
		// if(users.hasOwnProperty(user)){
		// 	io.to(socket.io.engine.id).emit("taken");
		// }
		if(users[user] == user){ //if username already exists
			socket.emit("taken");
		}
		else {
			users[user] = user;
			socket.user = user;
			// socket.emit('update', 'SERVER', 'You are live, ' + user);
			console.log(user + " is active");
			socket.room = "waiting";
			socket.join("waiting");
			// socket.broadcast.emit('update', 'SERVER', user + ' is live'); //can't see this?
			// io.sockets.emit('userUpdate', users);
			// io.sockets.in(socket.room).emit('updateRooms', rooms, socket.room);
			// console.log(socket.room);
			io.sockets.in("waiting").emit("updateRooms", rooms, socket.room);
			socket.emit("successConnect", user);
			// io.client.emit('updateRooms', rooms, socket.room);
		}
	});

	socket.on('addRoom', function(room, pass){
		console.log("here");
		rooms[room] = pass; //key=room, val=pass
		console.log(pass);
		// socket.room = room;
		socket.emit('update', 'SERVER', 'Room ' + room + ' is open'); //Can't see this?
		console.log(room + " is open");
		io.sockets.emit('addRoom', room, pass);
	});

	socket.on('dis', function(){
	    console.log("ready to leave");
		console.log(socket.user + " is not active");
        io.sockets.emit('userUpdate', users);
        delete users[socket.user];
		delete userRoom[socket.user];
        socket.broadcast.emit('update', 'SERVER', socket.user + ' has left');
		io.sockets.in(socket.room).emit('usersInRoom', userRoom, socket.room);
		socket.room = "waiting";
        socket.user = null;
		socket.emit('reload');


	});
    socket.on('disconnect', function(){
        console.log("ready to leave");
        console.log(socket.user + " is not active");
        // io.sockets.emit('userUpdate', users);
        delete users[socket.user];
		delete userRoom[socket.user];
        socket.broadcast.emit('update', 'SERVER', socket.user + ' has left');
		io.sockets.in(socket.room).emit('usersInRoom', userRoom, socket.room);

    });
    socket.on('enterRoom', function(roomName){
	    // console.log("Ready to enter room");
		socket.broadcast.to(socket.room).emit('update', 'SERVER', '' + socket.user + ' has left');
		userRoom[socket.user] = roomName;
		io.sockets.in(socket.room).emit('usersInRoom', userRoom, socket.room); // update the old room
        socket.leave(socket.room); //incase they are already in a room
        socket.room = roomName;
        socket.join(roomName);
		console.log(socket.user + " is in room " + roomName);
		// socket.broadcast.to(socket.room).emit('update', 'SERVER', '' + socket.user + ' is now here');
		io.sockets.in(socket.room).emit('updateRooms', rooms, roomName);
		io.sockets.in(socket.room).emit('usersInRoom', userRoom, roomName); //update the new room
		socket.emit("updateChatRoom", socket.user);

        // io.sockets.in(socket.room).emit()
	});

	socket.on('privMessage', function(usr, msg){
		var sender = socket.user;
		socket.broadcast.emit("privMessage", usr, msg, sender);
	});
	socket.on('removePrep', function(person) {
		socket.broadcast.to(socket.room).emit('update', 'SERVER', '' + socket.user + ' has been kicked');
		userRoom[person] = "waiting";
		io.sockets.in(socket.room).emit('usersInRoom', userRoom, socket.room); // update the old room
		io.sockets.emit("finRemove", person);
	});
	socket.on("finRemove", function(person){
		socket.leave(socket.room); //incase they are already in a room
		socket.room = "waiting";
		socket.join("waiting");
		console.log(socket.user + " is in room: waiting");
		// socket.broadcast.to(socket.room).emit('update', 'SERVER', '' + socket.user + ' is now here');
		io.sockets.in(socket.room).emit('updateRooms', rooms, "waiting");
		io.sockets.in("waiting").emit("usersInRoom", userRoom, "waiting");
		socket.emit("updateChatRoom", socket.user);
		// io.sockets.in(socket.room).emit()
	});
	socket.on("ban", function(person){
		socket.broadcast.to(socket.room).emit('update', 'SERVER', '' + socket.user + ' has been banned');
		userRoom[person] = "waiting";
		io.sockets.in(socket.room).emit('usersInRoom', userRoom, socket.room); // update the old room
		io.sockets.emit("finBan", person, socket.room);
	});
	socket.on("finBan", function(person){
		socket.leave(socket.room); //incase they are already in a room
		socket.room = "waiting";
		socket.join("waiting");
		console.log(socket.user + " is in room: waiting");
		// socket.broadcast.to(socket.room).emit('update', 'SERVER', '' + socket.user + ' is now here');
		io.sockets.in(socket.room).emit('updateRooms', rooms, "waiting");
		io.sockets.in("waiting").emit("usersInRoom", userRoom, "waiting");
		socket.emit("updateChatRoom", socket.user);
		// io.sockets.in(socket.room).emit()
	});



	// From https://code.tutsplus.com/tutorials/how-to-create-a-resumable-video-uploader-in-nodejs--net-25445
	socket.on('Upload', function (file) {
		//Received an image: broadcast to all
		console.log("Uploading");
		io.sockets.in(socket.room).emit('Upload', socket.user, file);
	});

	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
		// console.log("message: "+data["message"]); // log it to the Node.JS output
		io.sockets.in(socket.room).emit("message_to_client", socket.user, data) // broadcast the message to other users...... {message:data["message"] }
	});
});
