<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="style.css" />
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"> </script>
<script src = "/socket.io/socket.io.js"> </script>
<script type = "text/javascript">

var socketio = io.connect();
var username = "";
var rooms = {};




socketio.on("connect", function(addUser){
    username = prompt("What is your name?");
    alert("hi " + username + ", " + "you are active!");
    socketio.emit("addUser", username);

});

socketio.on('updateRooms', function(roomsArr, curRoom){ //key is roomname, value is password, don't need that right now
    $('#rooms').empty();
    $.each(roomsArr, function(key, value) {
        if (key == "waiting") {
        }
        else {
            if (key == curRoom) {
                $('#rooms').append('<div>' + key + '</div>');
                rooms[key] = value;
            }
            else {
                $('#rooms').append('<div><button onclick="enterRoom(\'' + key + '\')">' + key + '</button></div>');
                rooms[key] = value;
            }
        }
    });
});

socketio.on("taken", function(){
    alert("Username taken, try again");
});

socketio.on('usersInRoom', function(userRoom, curRoom){
    $("#users").empty();
    $("#users").append('Users in this room <br>');
    $.each(userRoom, function(key, value){
       if(value == curRoom){
           if(key != null && key!= "") {
               $('#users').append('<div><button onclick="privMessage(\'' + key + '\')">' + key + '</button></div>');
           }
       }
    });
});

socketio.on("addRoom", function(newRoom, pass){
    rooms[newRoom] = pass; //adding this room to the array for all users
    $('#rooms').append('<div><button onclick="enterRoom(\'' + newRoom + '\')">' + newRoom + '</button></div>');
});

socketio.on("message_to_client", function (user, data) {
    //Append an HR thematic break and the escaped HTML of the new message
    // document.getElementById("chatlog").appendChild(document.createElement("hr"));
    // document.getElementById("chatlog").appendChild(document.createTextNode(data['message']));
    $('#chatlog').append(user + ': ' + data + "<br>");
});
socketio.on("updateChatRoom", function () {
    //Append an HR thematic break and the escaped HTML of the new message
    // document.getElementById("chatlog").appendChild(document.createElement("hr"));
    // document.getElementById("chatlog").appendChild(document.createTextNode(data['message']));
    $('#chatlog').append(user + ': ' + data + "<br>");
});



socketio.on("userUpdate", function (users) { //in this case, the key and value are the same(from my array), so i just need to print the key
    $('#users').empty();
    // $.each(users, function(key, value) {                                    //displays the users online
    //     $("#users").append("<div data-user='" + key + "'>" + key + "</div>");
    // });
});

socketio.on("privMessage", function(user, mess, sender){
   if(user === username){
       alert("message from: " + sender + ": " +  mess);
   }
});
socketio.on("reload", function(){
    window.location.reload();
});

// function addUser(){
//     var newUser = $("#name").val();
//     username = $("#name").val();
//     socketio.emit("addUser", newUser);
// }

function addRoom(){
    var newRoom = $("#room").val();
    var pass = prompt("Make the room private? Add a password here, or click cancel to make it open!"); //by default, cancel sets pass = ""
    // alert(pass);
    socketio.emit("addRoom", newRoom, pass);
}

function sendMessage() {
    var msg = document.getElementById("message_input").value;
    socketio.emit("message_to_server", msg);
}



function disconnect(){
    // alert("Dis");
    username = null;
    socketio.emit("dis");
}

function enterRoom(roomName){
    // var room = jQuery(this).data('room');
    if(rooms[roomName] == "" || rooms[roomName] == null) {
        socketio.emit("enterRoom", roomName);
    }
    else{
        var pass = prompt("This room is private. What is the password?");
        if(rooms[roomName] == pass){
            socketio.emit("enterRoom", roomName);
        }
        else{
            alert("That was not the password, please try again");
        }
    }

}

function privMessage(user){
    var msg = prompt("Send your message");
    socketio.emit("privMessage", user, msg);
}

// var socket = io('/my-namespace');
// <input type="text" id="name" placeholder="Pick a username"/>
// <button onclick='addUser()'> submit </button><br/>

</script>
</head>
<body>


    <input type="text" id="room" placeholder="Make a new room?"/>
    <button onclick='addRoom()'> submit </button><br/>

    <input type="text" id="message_input"/>
    <button onclick='sendMessage()'> send </button>

    <button onclick='disconnect()'> disconnect? </button>

    <div id="chatlog"> </div>
    <div style="position:absolute; top:0px; right:10px" id="users"></div>
    <div style="margin-left:50%; position:absolute; top:0px" id="rooms"></div>
</body>
</html>
